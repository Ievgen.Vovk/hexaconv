"""
Tensorflow convolutional layers, suitable for operation on input images with hexagonal pixels, common in Imaging Atmospheric Cherenkov Telescope (IACT) data.

Layers
======

    HexaConv2D            -- hexagonal convolution layer
    HexaConv2DTranspose   -- transpose hexagonal convolution layer
    
Helper functions
================

    get_hexa_conv_mask  -- returns the 1 or 0 containing mask, representing the hexagonal of the given size
    find_max_hexagonal  -- returns the maximal symmetric hexagon size that fits into the given image

"""

from .hexaconv import HexaConv2D, HexaConv2DTranspose
from .hexaconv import get_hexa_conv_mask, find_max_hexagonal
from .mapper import ImageMapper

__all__ = [
    'HexaConv2D',
    'HexaConv2DTranspose',
    'get_hexa_conv_mask',
    'find_max_hexagonal',
    'ImageMapper'
]
