#!/usr/bin/env python
# coding=utf-8

# ===========================================================================
# Copyright 2018 Ievgen Vovk
# The code is distributed under the GPL license, please find its full text in
# the "COPYING" file.
# ===========================================================================

# Science
import numpy
import scipy

# Machine learning
import tensorflow as tf


def find_max_hexagonal(image, with_debug=False):
    """
    Returns the maximal size of the hexagon
    that fits into the given image. The image
    must be filled with ones at pixles where 
    the hexagon is allowed and with zeros otherwise.
    
    Parameters
    ----------
    image: ndarray
        2D image to fit the hexagon to.
    with_debug: bool
        Whether to return additional data, useful for debugging.
        
    Returns
    -------
    size: int
        Size of the hexagon in question.
    dual: bool
        Duality of the hexagon in question.
    hexagon_image: ndarray
        [If with_debug=True] 2D image of the hexagon, 
        reshaped to the image dimentions.
    cam_conv: ndarray
        [If with_debug=True] 2D convolution of the input image 
        with the maximal hexagon, normalized to 1.
    
    """
    
    # Normalizing the input
    image = image / image.max()
    
    im_size_x = image.shape[0]
    im_size_y = image.shape[1]

    # Scanning the possible hexagon sizes and dualities
    for _size in range(2, min(im_size_x, im_size_y)):
        for _dual in (False, True):
            hexagon = get_hexa_conv_mask(_size, dual=_dual)

            # This convolution results in hexagon.sum()
            # if the hexagon is fully contained in the image
            cam_conv = scipy.signal.convolve2d(image, hexagon, mode='valid')
            cam_conv /= hexagon.sum()

            wh_max = numpy.where(cam_conv == 1)
            
            # If there is at least 1 hexagon fitting,
            # taking the first one
            if wh_max[0].size:
                n_found = wh_max[0].size
                shift_x = wh_max[0][0]
                shift_y = wh_max[1][0]
                size = _size
                dual = _dual

    if with_debug:
        # Debug output
        
        # Maximal hexagon
        hexagon = get_hexa_conv_mask(size, dual=dual)
        
        # Its convolution
        cam_conv = scipy.signal.convolve2d(image, hexagon, mode='valid')
        cam_conv /= hexagon.sum()

        # Reshaping the hexagon to match the input image
        hexagon_image = numpy.pad(
            hexagon, 
            pad_width=[
                [0, im_size_x - hexagon.shape[0]],
                [0, im_size_y - hexagon.shape[1]]
            ]
        )
            
        # Aligning the hexagon
        hexagon_image = numpy.roll(hexagon_image, shift=(shift_x, shift_y), axis=(0, 1))
        
        return size, dual, hexagon_image, cam_conv
        
    else:
        return size, dual


def get_hexa_conv_mask(kernel_size, dual=False):
    """
    Generates the mask, appropriate for creating the square convolutional
    kernels for re-mapped hexagonal pixels.

    Parameters
    ----------
    kernel_size: int
        The size of the kernel.
    dual: bool, optional
        Whether to return the "dual" kernel mask. Dual kernels appear due to asymmetry of the
        kernels of even size; for odd kernel sizes original and dual kernel coincide.
        Defaults to False.

    Returns
    -------
    mask: ndarray
        Boolean mask of shape (kernel_size, kernel_size). Pixels masked
        as "False" should not take part in convolution.
    """

    if kernel_size == 2:
        mask = scipy.array([[1, 1],
                            [1, 0]], dtype=scipy.bool_)
    else:
        mask = scipy.zeros((kernel_size, kernel_size), dtype=scipy.bool_)
        sub_mask = get_hexa_conv_mask(kernel_size - 1)

        is_even = (kernel_size % 2 == 0)

        if is_even:
            mask[0:-1, 0:-1] = scipy.bitwise_or(mask[0:-1, 0:-1], sub_mask)
            mask[1:, 0:-1] = scipy.bitwise_or(mask[1:, 0:-1], sub_mask)
            mask[0:-1, 1:] = scipy.bitwise_or(mask[0:-1, 1:], sub_mask)
        else:
            mask[1:, 1:] = scipy.bitwise_or(mask[1:, 1:], sub_mask)
            mask[0:-1, 1:] = scipy.bitwise_or(mask[0:-1, 1:], sub_mask)
            mask[1:, 0:-1] = scipy.bitwise_or(mask[1:, 0:-1], sub_mask)

    if dual:
        mask = mask[::-1, ::-1]

    return mask


def get_hexa_conv_kernel(kernel_shape, dtype=tf.float32, name='kernel'):
    """
    Returns the kernel, suitable for convolutions of the remapped
    hexagonal pixels.

    Parameters
    ----------
    kernel_shape: tuple
        Shape of the kernel to return structured as (kernel_size, n_input_filters, n_output_filters).
    dtype: tensorflow.python.framework.dtypes.DType, optional
        Data type of the resulting kernel. Defaults to tf.float32.
    name: str
        The name of the kernel. Will be used for the name scope, where the kernel
        parts are created.

    Returns
    -------
    kernel: tensorflow.python.framework.ops.Tensor
        The kernel tensor of the shape (kernel_size, kernel_size, kernel_shape[1], kernel_shape[2]).
    """

    kernel_size = kernel_shape[0]

    # Retrieving the mask of the trainable pixels
    mask = get_hexa_conv_mask(kernel_size)

    with tf.name_scope(name):
        output_shape = (kernel_size, kernel_size) + kernel_shape[1:]
        # Creating the trainable part
        non_zero = tf.Variable(0.1 * tf.random_normal(shape=output_shape, dtype=dtype), name='nonzero_part')
        # non_zero = tf.Variable(tf.constant(1, shape=output_shape, dtype=dtype), name=name + '_nonzero')
        # Creating the non-trainable part
        zero = tf.constant(0, shape=output_shape, dtype=dtype, name='zero_part')

        # Creating the kernel
        kernel = tf.multiply(non_zero, tf.cast(mask[..., None, None], dtype=dtype)) + \
                 tf.multiply(zero, tf.cast(tf.logical_not(mask[..., None, None]), dtype=dtype))

    return kernel


class HexaConv2D(tf.keras.layers.Layer):
    """
    Functor that returns a square convolutional kernel, suitable for hexagonal 2D
    convolution of the input 4D tensors.
    """

    def __init__(self, filters, kernel_size,
                 strides=(1, 1, 1, 1), padding='same', activation='relu',
                 valid_input_mask=None, dual=False, trainable=True):
        """
        Constructs the functor for a specific kernel size and number of output filters.

        Reminder: the output layer sizes are:
            - for the "same" padding:
                out_height = ceil(float(in_height) / float(strides[1]))
                out_width  = ceil(float(in_width) / float(strides[2]))
            - for the "valid" padding:
                out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
                out_width  = ceil(float(in_width - filter_width + 1) / float(strides[2]))

        Parameters
        ----------
        filters: int
            The number of output filter layers.
        kernel_size: int
            The size of the square kernel. Must be >1.
        strides: tuple, optional
            4D tuple of strides - step along each input tensor dimensions.
            Defaults to (1, 1, 1, 1).
        padding: str, optional
            Padding to apply. Can be "same" or "valid". Case insensitive.
            Defaults to "same".
        activation: str, optional
            Activation to apply. Can be "relu", "sigmoid" or "none". Case insensitive.
            Defaults to "relu".
        valid_input_mask: tf.Tensor, optional
            Mask indicating the pixels, that should be used in the convolution operation.
            Pixels where mask == 0 are ignored - convolutions involving them are set to zero.
        dual: bool, optional
            Whether to use the "dual" kernel. Dual kernels appear due to asymmetry of the
            kernels of even size; for odd kernel sizes original and dual kernel coincide.
            Defaults to False.
        trainable: bool, optional
            Whether the resulting layer parameters should be trainable. Useful if the layer
            is loaded from a previously trained model. Defaults to False.
        """

        super(HexaConv2D, self).__init__()
        self.kernel_size = kernel_size
        self.filters = filters
        self.strides = strides
        self.padding = padding.upper()
        self.activation = activation.lower()
        self.valid_input_mask = valid_input_mask
        self.dual = dual
        self.trainable = trainable

        # Computing the valid output mask
        # This must be done here, so that the subsequent layers know it at the level of the network structure
        # prototyping - before the "build()" or "call()" methods are invoked.
        if self.valid_input_mask is not None:
            _mask = get_hexa_conv_mask(self.kernel_size, self.dual)
            kernel_mask = tf.constant(_mask[..., None, None], name='kernel_mask', dtype=self.valid_input_mask.dtype)

            valid_output_mask = tf.nn.conv2d(self.valid_input_mask, kernel_mask,
                                             strides=self.strides, padding=self.padding)
            valid_output_mask /= tf.reduce_sum(kernel_mask)
            self.valid_output_mask = tf.floor(valid_output_mask, name='valid_output_mask')
        else:
            self.valid_output_mask = None

        supported_activations = ["relu", "sigmoid", "none"]
        if self.activation not in supported_activations:
            raise ValueError("HexaConv2D: activation '{:s}' is not supported. Choices are {}."
                             .format(activation, supported_activations))

    def build(self, input_shape):
        """
        Builds the convolutional kernel for a specific input shape.

        Parameters
        ----------
        input_shape: tuple
            The shape of the input: (n_images, nx, ny, n_input_filters)

        Returns
        -------
        None
        """

        # kernel_shape = (self.kernel_size, input_shape[-1], self.num_outputs)
        # self.kernel = get_hexa_conv_kernel(kernel_shape)

        mask = get_hexa_conv_mask(self.kernel_size, self.dual)

        # Creating the trainable part
        kernel_shape = (self.kernel_size, self.kernel_size, int(input_shape[-1]),  self.filters)
        trainable_part = self.add_variable("trainable",
                                           shape=kernel_shape,
                                           regularizer='l2',
                                           initializer=tf.random_normal_initializer(mean=0, stddev=0.05),
                                           trainable=self.trainable)

        self.kernel = tf.multiply(trainable_part,
                                  tf.cast(mask[..., None, None], dtype=trainable_part.dtype),
                                  name='kernel')

        self.bias = self.add_variable("bias",
                                      shape=[self.filters],
                                      initializer=tf.zeros_initializer,
                                      trainable=self.trainable)

        # self.losses = tf.reduce_sum(self.kernel ** 2)
        self.add_loss(tf.reduce_sum(self.kernel ** 2))
        self.add_loss(tf.reduce_sum(self.bias ** 2))

    def call(self, inputs, *args, **kwargs):
        """
        Returns the hexagonal convolution operation, including the added bias and ReLU activation.

        Parameters
        ----------
        inputs: tf.Tensor
            Input 4D tensor.

        Returns
        -------
        tf.Operation:
            Resulting operation.
        """

        conv = tf.nn.conv2d(inputs, self.kernel, strides=self.strides, padding=self.padding)

        if self.activation == "relu":
            result = tf.nn.relu(tf.nn.bias_add(conv, self.bias))
        elif self.activation == "sigmoid":
            result = tf.nn.sigmoid(tf.nn.bias_add(conv, self.bias))
        else:
            result = tf.nn.bias_add(conv, self.bias)

        if self.valid_output_mask is not None:
            result = result * self.valid_output_mask

        return result


class HexaConv2DTranspose(tf.keras.layers.Layer):
    """
    Functor that returns a square transposed convolution kernel, suitable for hexagonal 2D
    convolution of the input 4D tensors.
    """

    def __init__(self, filters, kernel_size,
                 strides=(1, 1, 1, 1), padding='same', activation='relu',
                 dual=False, trainable=True):
        """
        Constructs the functor for a specific kernel size and number of output filters.

        Reminder: the output layer sizes are:
            - for the "same" padding:
                out_height = (in_height - 1) * strides[1] + 1
                out_width  = (in_width - 1) * strides[2] + 1
            - for the "valid" padding:
                out_height = (in_height - 1) * strides[1] + filter_height
                out_width  = (in_width - 1)* strides[2] + filter_width

        Parameters
        ----------
        filters: int
            The number of output filter layers.
        kernel_size: int
            The size of the square kernel. Must be >1.
        strides: tuple, optional
            4D tuple of strides - step along each input tensor dimensions.
            Defaults to (1, 1, 1, 1).
        padding: str, optional
            Padding to apply. May be "same" or "valid". Case insensitive.
            Defaults to "same".
        activation: str, optional
            Activation to apply. Can be "relu", "sigmoid" or "none". Case insensitive.
            Defaults to "relu".
        dual: bool, optional
            Whether to use the "dual" kernel. Dual kernels appear due to asymmetry of the
            kernels of even size; for odd kernel sizes original and dual kernel coincide.
            Defaults to False.
        trainable: bool, optional
            Whether the resulting layer parameters should be trainable. Useful if the layer
            is loaded from a previously trained model. Defaults to False.
        """

        super(HexaConv2DTranspose, self).__init__()
        self.kernel_size = kernel_size
        self.filters = filters
        self.strides = strides
        self.padding = padding.upper()
        self.activation = activation.lower()
        self.dual = dual
        self.trainable = trainable

        supported_activations = ["relu", "sigmoid", "none"]
        if self.activation not in supported_activations:
            raise ValueError("HexaConv2DTranspose: activation '{:s}' is not supported. Choices are {}."
                             .format(activation, supported_activations))

    def build(self, input_shape):
        """
        Builds the convolutional kernel for a specific input shape.

        Parameters
        ----------
        input_shape: tuple
            The shape of the input: (n_images, nx, ny, n_input_filters)

        Returns
        -------
        None
        """

        # kernel_shape = (self.kernel_size, input_shape[-1], self.num_outputs)
        # self.kernel = get_hexa_conv_kernel(kernel_shape)

        mask = get_hexa_conv_mask(self.kernel_size, self.dual)

        # Creating the trainable part
        kernel_shape = (self.kernel_size, self.kernel_size, self.filters, int(input_shape[-1]))
        trainable_part = self.add_variable("trainable",
                                           shape=kernel_shape,
                                           regularizer='l2',
                                           initializer=tf.random_normal_initializer(mean=0, stddev=0.05),
                                           trainable=self.trainable)

        self.kernel = tf.multiply(trainable_part,
                                  tf.cast(mask[..., None, None], dtype=trainable_part.dtype),
                                  name='kernel')

        self.bias = self.add_variable("bias",
                                      shape=[self.filters],
                                      initializer=tf.zeros_initializer,
                                      trainable=self.trainable)

        # self.losses = tf.reduce_sum(self.kernel ** 2)
        self.add_loss(tf.reduce_sum(self.kernel ** 2))
        self.add_loss(tf.reduce_sum(self.bias ** 2))

    def call(self, inputs, *args, **kwargs):
        """
        Returns the hexagonal transpose convolution operation, including the added bias and ReLU activation.

        Parameters
        ----------
        inputs: tf.Tensor
            Input 4D tensor.

        Returns
        -------
        tf.Operation:
            Resulting operation.
        """

        if self.padding == 'VALID':
            output_size_h = (inputs.shape[1] - 1) * self.strides[1] + self.kernel_size
            output_size_w = (inputs.shape[2] - 1) * self.strides[2] + self.kernel_size
        elif self.padding == 'SAME':
            output_size_h = (inputs.shape[1] - 1) * self.strides[1] + 1
            output_size_w = (inputs.shape[2] - 1) * self.strides[2] + 1
        else:
            raise ValueError("Unknown padding")

        output_shape = tf.stack([tf.shape(inputs)[0],
                                 output_size_h, output_size_w,
                                 self.filters])

        conv = tf.nn.conv2d_transpose(inputs, self.kernel,
                                      output_shape=output_shape,
                                      strides=self.strides,
                                      padding=self.padding)

        if self.activation == "relu":
            result = tf.nn.relu(tf.nn.bias_add(conv, self.bias))
        elif self.activation == "sigmoid":
            result = tf.nn.sigmoid(tf.nn.bias_add(conv, self.bias))
        else:
            result = tf.nn.bias_add(conv, self.bias)

        return result


class HexaPool2D(tf.keras.layers.Layer):
    """
    Functor that returns a square max pooling operation, suitable for hexagonal 2D
    convolution of the input 4D tensors.
    """

    def __init__(self, kernel_size, strides=(1, 1, 1, 1), padding='same', mode='max'):
        """
        Constructs the functor for a specific pooling kernel size, strides and padding.

        Parameters
        ----------
        kernel_size: int
            The size of the square kernel. Must be >1.
        strides: tuple, optional
            4D tuple of strides - step along each input tensor dimensions.
            Defaults to (1, 1, 1, 1).
        padding: str, optional
            Padding to apply. May be "same" or "valid". Case insensitive.
            Defaults to "same".
        mode: str, optional
            Pooling mode. Can be either "max" or "average". Case insensitive.
            Defaults to "max".
        """

        super(HexaPool2D, self).__init__()
        self.kernel_size = kernel_size
        self.ksizes = (1, kernel_size, kernel_size, 1)
        self.strides = strides
        self.padding = padding.upper()
        self.rates = (1, 1, 1, 1)
        self.mode = mode.upper()

    def build(self, input_shape):
        """
        Builds the pooling mask.

        Parameters
        ----------
        input_shape: tuple
            The shape of the input: (n_images, nx, ny, n_input_filters)

        Returns
        -------
        None
        """

        self.mask = get_hexa_conv_mask(self.kernel_size)

    def call(self, inputs):
        """
        Returns the hexagonal pooling operation.

        Parameters
        ----------
        inputs: tf.Tensor
            Input 4D tensor.

        Returns
        -------
        tf.Operation:
            Resulting operation.
        """

        patches = tf.extract_image_patches(inputs,
                                           ksizes=self.ksizes,
                                           strides=self.strides,
                                           padding=self.padding,
                                           rates=self.rates)

        shape_ = tuple(patches.shape[:-1]) + (inputs.shape[-1],) + (self.kernel_size, self.kernel_size)
        patches = tf.reshape(patches, shape_)

        masked = patches * tf.cast(self.mask[None, None, None, None, ...],
                                   dtype=patches.dtype)

        if self.mode == "MAX":
            masked_pool = tf.reduce_max(masked, axis=(-1, -2))
        elif self.mode == "AVERAGE":
            masked_pool = tf.reduce_mean(masked, axis=(-1, -2))
        else:
            raise ValueError("HexaPool2D: invalid specified mode {}. Must be \"max\" or \"average\"."
                             .format(self.mode))

        return masked_pool
