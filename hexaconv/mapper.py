#!/usr/bin/env python
# coding=utf-8

# ===========================================================================
# Copyright 2018 Ievgen Vovk
# The code is distributed under the GPL license, please find its full text in
# the "COPYING" file.
# ===========================================================================


# Science
import scipy
import h5py

from .hexaconv import get_hexa_conv_mask

# Plotting
from matplotlib import pyplot


class ImageMapper:
    def __init__(self, pixel_mapping_file, use_valid_hexagon=False):
        self.pixel_mapping_file = pixel_mapping_file
        self.use_valid_hexagon = use_valid_hexagon

        self.pixel_coordinates = self._read_pixel_coordinates(pixel_mapping_file)
        self.grid = self._read_grid_parameters(pixel_mapping_file)
        
        if use_valid_hexagon:
            self.valid_hexagon_map = self._read_valid_hexagon_map(pixel_mapping_file)
            
            for kind in self.pixel_coordinates:
                for xy in self.pixel_coordinates[kind]:
                    self.pixel_coordinates[kind][xy] = self.pixel_coordinates[kind][xy][self.valid_hexagon_map]
        else:
            self.valid_hexagon_map = scipy.ones_like(self.pixel_coordinates['remapped']['x'])

        self._n_pixels = len(self.pixel_coordinates['original']['x'])
        self._n_useful_pixels = self._read_n_useful_pixels(pixel_mapping_file)

        self._nx_remapped = len(scipy.unique(self.pixel_coordinates['remapped']['x']))
        self._ny_remapped = len(scipy.unique(self.pixel_coordinates['remapped']['y']))

        self._min_x_remapped = self.pixel_coordinates['remapped']['x'].min()
        self._min_y_remapped = self.pixel_coordinates['remapped']['y'].min()

    @property
    def n_pixels(self):
        return self._n_pixels

    @property
    def n_useful_pixels(self):
        return self._n_useful_pixels

    @property
    def nx_remapped_pixels(self):
        return self._nx_remapped

    @property
    def ny_remapped_pixels(self):
        return self._ny_remapped

    @staticmethod
    def _read_pixel_coordinates(pixel_mapping_file_name):
        pixel_coordinates = {'original': {'x': [], 'y': []},
                             'rotated': {'x': [], 'y': []},
                             'remapped': {'x': [], 'y': []}}

        with h5py.File(pixel_mapping_file_name, 'r') as input_data:
            pixel_coordinates['original']['x'] = input_data['/original/x'].value
            pixel_coordinates['original']['y'] = input_data['/original/y'].value
            pixel_coordinates['rotated']['x'] = input_data['/rotated/x'].value
            pixel_coordinates['rotated']['y'] = input_data['/rotated/y'].value
            pixel_coordinates['remapped']['x'] = input_data['/remapped/x'].value
            pixel_coordinates['remapped']['y'] = input_data['/remapped/y'].value

        return pixel_coordinates

    @staticmethod
    def _read_n_useful_pixels(pixel_mapping_file_name):
        with h5py.File(pixel_mapping_file_name, 'r') as input_data:
            n_useful_pixels = input_data['n_useful_pixels'].value

        return n_useful_pixels

    @staticmethod
    def _read_grid_parameters(pixel_mapping_file_name):
        grid = {'size': {}, 'extent': {}}

        with h5py.File(pixel_mapping_file_name, 'r') as input_data:
            grid['size']['x'] = input_data['/hexa_grid/size/x'].value
            grid['size']['y'] = input_data['/hexa_grid/size/y'].value

            grid['extent']['x_min'] = input_data['/hexa_grid/extent/x_min'].value
            grid['extent']['x_max'] = input_data['/hexa_grid/extent/x_max'].value
            grid['extent']['y_min'] = input_data['/hexa_grid/extent/y_min'].value
            grid['extent']['y_max'] = input_data['/hexa_grid/extent/y_max'].value

        return grid
    
    @staticmethod
    def _read_valid_hexagon_map(pixel_mapping_file_name):
        with h5py.File(pixel_mapping_file_name, 'r') as input_data:
            valid_hexagon_map = input_data['/valid_hexagon_map'].value

        return valid_hexagon_map

    def remap_pixels(self, pixel_data):
        if self.use_valid_hexagon:
            pixel_data = pixel_data[self.valid_hexagon_map]
        
        remapped = scipy.zeros((self._nx_remapped, self._ny_remapped))
            
        remapped[self.pixel_coordinates['remapped']['x'] + self._min_x_remapped - 1,
                 self.pixel_coordinates['remapped']['y'] + self._min_y_remapped - 1] = pixel_data

        return remapped

    def reverse_remap_pixels(self, pixel_map):
        reverse_remapped = pixel_map[self.pixel_coordinates['remapped']['x'] + self._min_x_remapped - 1,
                                     self.pixel_coordinates['remapped']['y'] + self._min_y_remapped - 1]
        
        if self.use_valid_hexagon:
            place_holder = scipy.zeros_like(self.valid_hexagon_map)
            place_holder[self.valid_hexagon_map] = reverse_remapped
            reverse_remapped = place_holder

        return reverse_remapped

    def remapped_signal_mask(self):
        """Returns the mask of signal pixels in the re-mapped camera representation"""

        mask = scipy.zeros((self._nx_remapped, self._ny_remapped))

        # Counting how many times a given pixel was used (can be more than once)
        for x, y in zip(self.pixel_coordinates['remapped']['x'] + self._min_x_remapped - 1,
                        self.pixel_coordinates['remapped']['y'] + self._min_y_remapped - 1):
            mask[x, y] += 1

        # Excluding the pixels used more than once
        mask[mask > 1] = 0

        return mask

    def display_rotated(self, pixel_data, ax=None, **kwargs):
        if ax is None:
            ax = pyplot.gca()
            
        if self.use_valid_hexagon:
            pixel_data = pixel_data[self.valid_hexagon_map]

        ax.set_xlabel('Rotated X, mm')
        ax.set_ylabel('Rotated Y, mm')
        data = ax.hexbin(self.pixel_coordinates['rotated']['x'], self.pixel_coordinates['rotated']['y'], pixel_data,
                         gridsize=(self.grid['size']['x'], self.grid['size']['y']),
                         extent=(self.grid['extent']['x_min'], self.grid['extent']['x_max'],
                                 self.grid['extent']['y_min'], self.grid['extent']['y_max']
                                 ),
                         **kwargs)
        pyplot.colorbar(data)

        return data

    def display_remapped(self, pixel_data, ax=None, **kwargs):
        if self.use_valid_hexagon:
            pixel_data = pixel_data[self.valid_hexagon_map]
        
        remapped, x_edges, y_edges = scipy.histogram2d(self.pixel_coordinates['remapped']['x'],
                                                       self.pixel_coordinates['remapped']['y'],
                                                       weights=pixel_data,
                                                       range=((-self.nx_remapped_pixels/2, self.nx_remapped_pixels/2),
                                                              (-self.ny_remapped_pixels/2, self.ny_remapped_pixels/2)),
                                                       bins=(self.nx_remapped_pixels, self.ny_remapped_pixels))

        if ax is None:
            ax = pyplot.gca()

        ax.set_xlabel('Re-mapped bin X')
        ax.set_ylabel('Re-mapped bin Y')
        data = ax.pcolormesh(x_edges, y_edges, remapped.transpose(), **kwargs)
        pyplot.colorbar(data)

        return data

    def display_contours_rotated(self, pixel_data, ax=None, **kwargs):
        if ax is None:
            ax = pyplot.gca()

        if self.use_valid_hexagon:
            pixel_data = pixel_data[self.valid_hexagon_map]

        ax.set_xlabel('Rotated X, mm')
        ax.set_ylabel('Rotated Y, mm')
        data = ax.tricontour(self.pixel_coordinates['rotated']['x'],
                             self.pixel_coordinates['rotated']['y'],
                             pixel_data,
                             **kwargs)

        return data

    def display_contours_remapped(self, pixel_data, ax=None, **kwargs):
        if ax is None:
            ax = pyplot.gca()

        if self.use_valid_hexagon:
            pixel_data = pixel_data[self.valid_hexagon_map]

        ax.set_xlabel('Re-mapped bin X')
        ax.set_ylabel('Re-mapped bin Y')
        data = ax.tricontour(self.pixel_coordinates['remapped']['x'],
                             self.pixel_coordinates['remapped']['y'],
                             pixel_data,
                             **kwargs)

        return data
