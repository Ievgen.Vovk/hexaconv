## HexaConv module for deep-learning with Tensorflow

This package provides a convolutional layers, suitable for operation on input images with hexagonal pixels, common in Imaging Atmospheric Cherenkov Telescope (IACT) data.

### Installation

```bash
git clone 
cd hexaconv
pip install .
```

### Usage

```python
import hexaconv
...
layer = hexaconv.HexaConv2D(64, 2, padding='valid')
```
