from setuptools import setup

setup(name='HexaConv',
      description='Hexagonal convolution module for Tensorflow',
      version='0.1',
      author='Ievgen Vovk',
      author_email='vovk@icrr.u-tokyo.ac.jp',
      long_description=open("README.md").read(),
      url='http://gitlab.com/',
      license='LICENSE.txt',
      packages=['hexaconv'],
      install_requires=[
        "tensorflow",
      ])
